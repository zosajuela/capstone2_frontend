// console.log("hello from JS"); 

//THE first thing that we need to do is to identify which course it needs to display inside the browser. 
//we are going to use the course id to identify the correct course properly. 
let params = new URLSearchParams(window.location.search)
//window.location -> returns a location object with information about the "current" location of the document. 
// .search => contains the query string section of the current URL. //search property returns an object of type stringString.
//  URLSearchParams() -> this method/constructor creates and returns a URLSearchParams object.  (this is a class)
//"URLSearchParams" -> this describes the interface that defines utility methods to work with the query string of a URL.  (this is the prop type). 
// new -> instantiate a user-defined object. 
//yes it is a class -> template for creating the object.

//params = {
//  "courseId": "....id ng course that we passed"   
//}
let id = params.get('courseId')
console.log(id)

//lets capture the sections of the html body
let name = document.querySelector("#courseName")
let desc = document.querySelector("#courseDesc")
let price = document.querySelector("#coursePrice")

fetch(`http://localhost:4000/api/courses/${id}`).then(res => res.json()).then(data => {
    console.log(data)

    name.innerHTML = data.name
    desc.innerHTML = data.description
    price.innerHTML = data.price    
}) //if the result is displayed in the console. it means you were able to properly pass the courseid and successfully created your fetch request. 

document.querySelector("#deleted").addEventListener("click", () =>{
    const deleteMethod = {
        method: 'DELETE', // Method itself
        headers: {
         'Content-type': 'application/json; charset=UTF-8' // Indicates the content 
        },
        // No need to have body, because we don't send nothing to the server.
       }
       // Make the HTTP Delete call using fetch api
       fetch(`http://localhost:4000/api/courses/delete/${id}`,{
        method: 'DELETE',
        })
        .then(res => {
            return res.json()
         }).then(data => {
               location.reload()
                if(data === true){
               alert("successfully remove Course, please refresh the page")
            } else{
                 alert("successfully remove Course, please refresh the page")
           }
      })
    })  